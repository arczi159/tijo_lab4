package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.domain.repository.MovieRepository;

import java.util.ArrayList;
import java.util.List;

@Component
public class MovieListMapper {

    public List<MovieDto> mapToDto(List<Movie> movies) {
        List<MovieDto> moviesDto = new ArrayList<>();

        for(Movie movie: movies) {
            MovieDto movieDto = new MovieDto();

            movieDto.setMovieId(movie.getMovieId());
            movieDto.setTitle(movie.getTitle());
            movieDto.setImage(movie.getImage());
            movieDto.setYear(movie.getYear());
            moviesDto.add(movieDto);

        }

        return moviesDto;
    }

    public Movie mapCreateMovieDtoToMovie(CreateMovieDto createMovieDto){
        Movie movie = new Movie();
        movie.setYear(createMovieDto.getYear());
        movie.setImage(createMovieDto.getImage());
        movie.setTitle(createMovieDto.getTitle());

        return movie;
    }

    public Movie getMovieById(String idStr, List<Movie> all){
        int id = Integer.parseInt(idStr);
        return all.stream().filter(movie -> movie.getMovieId()==id)
                .findFirst()
                .get();
    }
}
